sample = {
    'AvgRateBands': {
        '500': 12.8,
        '1000': 11.5,
        '2000': 10.8
    },
    'PlanCode': 'TX9261',
    'Utility': 'TN-E',
    'TermType': 'Fixed',
    'Term': 24,
    'UnitOfMeasure': 'kWh',
    'Rate': 0.063,
    'EarlyTerminationPenalty': 150.0,
    'IsGreen': False,
    'DetailsByLanguage': [
        {
            'Language': 'English',
            'Title': 'Power of Texas 24',
            'Description': 'Lock in and get satisfaction with no rising rate for 24 months. Let us show you our excellent customer service!',
            'Documents': [
                {
                    'Name': 'Energy Facts Label',
                    'URL': 'https://s3-us-weions/PRO190401_eng_e2e91c2c-a30e-4558-9c47-e88a8f8cef72.pdf',
                    'Type': 'EnergyFactsLabel'
                },
                {
                    'Name': 'Terms Of Service',
                    'URL': 'https://sTMARKETING/POTX.TOS.02062019.English_ba6636a7-d928-4ae4-8666-89afa7ed47cf.pdf',
                    'Type': 'TermsOfService'
                },
                {
                    'Name': 'Your Rights as a Customer',
                    'URL': 'https://s3-us-wTING/POTX.YRAC.021b30f-47d8-8c88-75539bf4f301.pdf',
                    'Type': 'YourRightsAsCustomer'
                }
            ],
            'SubTitle': '24 Months of Price Security'
        },
        {
            'Language': 'Spanish',
            'Title': 'Power of Texas 24',
            'Description': 'Asegure una tarifa y tu satisfacción sin tarifas crecientes por 24 meses. ¡Te demostraremos nuestro servicio al cliente excelente!',
            'Documents': [
                {
                    'Name': 'Etiqueta de información sobre la Electricidad',
                    'URL': 'https://s3-us-west-2.amazonawXN3F24C0DG008_20190401_spa_e0a52dc3-7bae-44c2-a216-8ffe8f42287e.pdf',
                    'Type': 'EnergyFactsLabel'
                },
                {
                    'Name': 'Términos del Servicio',
                    'URL': 'https://s3-us-west-2.amazonaws.c/POTX.TOS.02062019.Spanish_f9a0eada-fa87-4198-af77-42675d1c0944.pdf',
                    'Type': 'TermsOfService'
                },
                {
                    'Name': 'Sus Derechos como Cliente',
                    'URL': 'https://s3-us-wDUCTMARKETING/POTX.YRAC.02132019.Spanish_915f9989-6134-4857-8013-62b50fe6ef8d.pdf',
                    'Type': 'YourRightsAsCustomer'
                }
            ],
            'SubTitle': '24 meses de precios asegurados'
        }
    ],
    'ServiceFee': 4.95,
    'ServiceFeeType': 'MONTHLY'
}

{'Your Rights as a Customer':'https://s3-us-wTING/POTX.YRAC.021b30f-47d8-8c88-75539bf4f301.pdf',
 'Sus Derechos como Cliente':'https://s3-us-wDUCTMARKETING/POTX.YRAC.02132019.Spanish_915f9989-6134-4857-8013-62b50fe6ef8d.pdf'}