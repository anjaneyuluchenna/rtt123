# import generator_1
from generator_1 import multi, var1

# print(multi(2,15))
print(var1)


"""
1) "aabbbcccaaacbddd"
expected output :
{
    "a": 2,
    "b": 3,
    "c": 3,
    "d": 3
}
2) "al@#f$k*" 
expected output :
"kf@#l$a*"

"a$^fq"  -----> q$^fa
"""
